using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CrawlerCreature Creature;

    // Start is called before the first frame update
    void Start()
    {
        if (Creature == null)
            Creature = gameObject.GetComponent<CrawlerCreature>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        move = transform.TransformDirection(move);
        float moveMag = Mathf.Clamp(move.magnitude, 0, 1);
        if (moveMag == 0)
            return;
        move.Normalize();

        Creature.Move(move, moveMag);
    }
}
