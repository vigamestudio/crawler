using UnityEngine;

public class CrawlerCreature : MonoBehaviour
{
    public float MoveStepSize = 0.5f;
    public float MaxTurnAngle = 0.1f;

    public float BodyRadius = 0.5f;

    public float WalkerOffset = 0.5f;

    public int ShowRay = 0;

    public bool EnableMovement = false;
    public bool EnablePath = false;

    [SerializeField]
    private int NumStepsAhead = 10;

    [SerializeField]
    private GameObject WalkerPrefab;
    [SerializeField]
    private int NumOfWalkers = 1;

    private GameObject[] _walkerChain;

    private void Start()
    {
        _walkerChain = new GameObject[NumOfWalkers];
        for (int i = 0; i < NumOfWalkers; i++)
        {
            _walkerChain[i] = Instantiate(WalkerPrefab);

            var prev = i == 0 ? gameObject : _walkerChain[i - 1];
            var curr = _walkerChain[i];
            curr.transform.position = prev.transform.position - prev.transform.forward * WalkerOffset;
        }
    }

    public void Move(Vector3 move, float moveMag)
    {
        if (EnablePath)
        {
            DrawPath(move, moveMag);
        }

        if (EnableMovement)
        {
            Vector3 position = transform.position;
            Vector3 direction = transform.forward;
            GetNextPosition(ref position, ref direction, move, moveMag);
            transform.position = position;
            transform.rotation = Quaternion.LookRotation(direction);
        }

        for (int i = 0; i < NumOfWalkers; i++)
        {
            var prev = i == 0 ? gameObject : _walkerChain[i - 1];
            var curr = _walkerChain[i];

            var toPrev = (prev.transform.position - curr.transform.position).normalized;
            curr.transform.position = prev.transform.position - toPrev * WalkerOffset;
            curr.transform.rotation = Quaternion.LookRotation(toPrev);
        }
    }

    private void GetNextPosition(ref Vector3 position, ref Vector3 direction, Vector3 move, float moveMag)
    {
        SampleGround(position, direction, out Vector3 ground, out Vector3 normal);

        /*if (normal != Vector3.up)
        {
            var right = Vector3.Cross(Vector3.up, normal).normalized;
            var forward = Vector3.Cross(normal, right).normalized;
            var up = Vector3.Cross(right, forward).normalized;

            Debug.DrawRay(transform.position, right, Color.red);
            Debug.DrawRay(transform.position, up, Color.green);
            Debug.DrawRay(transform.position, forward, Color.blue);

            move = Quaternion.LookRotation(forward) * move;
        }*/

        //Debug.DrawRay(transform.position, move);

        move = Vector3.ProjectOnPlane(move, normal).normalized;
        position = ground + Vector3.up * BodyRadius;

        float dot = Mathf.Clamp(Vector3.Dot(direction, move), -1, +1);
        Vector3 cross = Vector3.Cross(direction, move).normalized;
        if (cross.sqrMagnitude == 0)
        {
            cross = transform.up;
        }

        float angle = Mathf.Rad2Deg * Mathf.Acos(dot);
        angle = Mathf.Clamp(angle, -MaxTurnAngle, +MaxTurnAngle);
        if (angle > 0)
        {
            direction = Quaternion.AngleAxis(angle, cross) * direction;
        }

        //Debug.DrawRay(position, cross, Color.green);
        //Debug.DrawRay(position, direction, Color.blue);

        position += direction * MoveStepSize * moveMag * Time.deltaTime;
    }

    private void SampleGround(Vector3 position, Vector3 direction, out Vector3 ground, out Vector3 normal)
    {
        ground = position + Vector3.down;
        normal = Vector3.up;

        if (Physics.Raycast(position, Vector3.down, out RaycastHit hit2, BodyRadius * 2))
        {
            ground = hit2.point;
            normal = hit2.normal;
            return;
        }

        //Vector3[] samples = new Vector3[4];
        //samples[0] = Vector3.forward;
        //samples[1] = Vector3.down;
        //samples[2] = Vector3.back;
        //samples[3] = Vector3.up;
        //
        //for (int i = 0; i < samples.Length; i++)
        //{
        //    var sample = samples[i];
        //    if (Physics.Raycast(position, sample, out RaycastHit hit, BodyRadius * 2))
        //    {
        //        ground = hit.point;
        //        normal = hit.normal;
        //        return;
        //    }
        //}
    }

    private void DrawPath(Vector3 move, float moveMag)
    {
        Vector3 position = transform.position;
        Vector3 direction = transform.forward;
        for (int i = 0; i < NumStepsAhead; i++)
        {
            GetNextPosition(ref position, ref direction, move, moveMag);

            Vector3 right = Vector3.Cross(direction, transform.up);
            Vector3 a = position + right * 0.1f;
            Vector3 b = position - right * 0.1f;
            DebugExtension.DebugWireSphere(a, Color.green, 0.01f);
            DebugExtension.DebugWireSphere(b, Color.green, 0.01f);
            Debug.DrawLine(a, b, Color.green);
        }
    }
}
