using UnityEngine;

public class AiController : MonoBehaviour
{
    public CrawlerCreature Creature;

    public float AccelerationRate = 1.0f;

    [SerializeField]private float _acceleration = 0.0f;
    [SerializeField]private float _angle = 0.0f;
    [SerializeField]private Vector3 _move = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        if (Creature == null)
            Creature = gameObject.GetComponent<CrawlerCreature>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _acceleration = Random.Range(-AccelerationRate, +AccelerationRate);
        _move += Quaternion.AngleAxis(_acceleration, Vector3.up) * Vector3.forward;

        var move = _move;
        move = transform.TransformDirection(move);
        float moveMag = Mathf.Clamp(move.magnitude, 0, 1);
        if (moveMag == 0)
            return;
        move.Normalize();

        Creature.Move(move, moveMag);
    }
}
