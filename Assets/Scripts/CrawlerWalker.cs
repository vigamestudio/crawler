using UnityEngine;

public class CrawlerWalker : MonoBehaviour
{
    public float LegAngleThreshold = 0.8f;

    [SerializeField]
    private Transform _leftLeg;
    [SerializeField]
    private Transform _rightLeg;

    Vector3 _leftIk;
    Vector3 _rightIk;

    private void Start()
    {
        _leftIk = _leftLeg.position + Vector3.down;
        _rightIk = _rightLeg.position + Vector3.down;
    }

    private void FixedUpdate()
    {
        DebugExtension.DebugWireSphere(_leftLeg.position, Color.blue, 0.1f);
        DebugExtension.DebugWireSphere(_rightLeg.position, Color.blue, 0.1f);
        
        Debug.DrawLine(_leftLeg.position, _leftIk, Color.black);
        DebugExtension.DebugWireSphere(_leftIk, Color.red, 0.1f);
        
        Debug.DrawLine(_rightLeg.position, _rightIk, Color.black);
        DebugExtension.DebugWireSphere(_rightIk, Color.red, 0.1f);

        UpdateLeg(_leftLeg, ref _leftIk);
        UpdateLeg(_rightLeg, ref _rightIk);
    }

    private void UpdateLeg(Transform leg, ref Vector3 ik)
    {
        if (Physics.Raycast(leg.position, -transform.up, out RaycastHit hit))
        {
            //DebugExtension.DebugWireSphere(hit.point, Color.green, 0.05f);

            var toIk = (leg.position - ik).normalized;
            var toHit = (leg.position - hit.point).normalized;
            float dot = Vector3.Dot(toIk, toHit);

            if (dot <= LegAngleThreshold)
            {
                ik = hit.point;
            }
        }
    }
}
